# JP Moka Test
	For Testing Purpose with MUTEX . 
	There are 2 command With / Without Mutex, It will show 2 different method with same process.
	req_n_mutex(space)(int)		(Run request Concurrently without MUTEX Implementation)
	req_w_mutex(space)(int) 	(Run request Concurrently with MUTEX Implementation)
	You can directly run from main.go or using package bin/jp_mokatest
	Please install go before execute the commands
## Reference 1 
	go run src/jp_mokatest/main.go req_n_mutex 5	
	(Will run 5 Request Concurrently without MUTEX Implementation)

## Reference 2 
	bin/jp_mokatest req_w_mutex 3
	(Will run 3 Request Concurrently with MUTEX Implementation)

## Thank You

