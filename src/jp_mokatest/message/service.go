package message

import (
	"errors"
	"strings"
)

var (
	Help = errors.New(strings.TrimLeft(`
JP Moka Test.
Usage:
	For Testing Purpose with MUTEX . 
	There are 2 command With / Without Mutex, It will show 2 different method with same process.
The commands are:
	req_n_mutex(space)(int)		(Run request Concurrently without MUTEX Implementation)
	req_w_mutex(space)(int) 	(Run request Concurrently with MUTEX Implementation)
Example:
	req_n_mutex 5			(Will run 5 Request Concurrently without MUTEX Implementation)
	req_w_mutex 3			(Will run 3 Request Concurrently with MUTEX Implementation)
End Usage;
	`, "\n"))
	
	ErrWrongArg       = errors.New("Error : Wrong Command Requested")
	ErrSecondArg      = errors.New("Error : Second Parameter Must Be int")
	ComStart		= "======> Start %v request concurrently \n"
	ComFinish		= "======> Request Ended"
	ComReqStart		= "=== Request R%v start at = %v \n"
	ComReqFinish	= "=== Request R%v Finish at = %v \n"
	ComReqAnswer	= "=== R%v | StartTime : %v | FinishTime : %v \n"

)

