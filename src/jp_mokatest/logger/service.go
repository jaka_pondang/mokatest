package log

import (
	"fmt"
	m "jp_mokatest/message"
)

// Interface For Processing Request
type Logger interface {  
	Start()
	Finish()
}
// Start & Finish Type Without Mutex
type WithoutMutex struct {  
	RequestId int
	Timestamp int64
}

// Log Start Without Mutex 
func (p WithoutMutex) Start() { 

	fmt.Printf(m.ComReqStart, p.RequestId, p.Timestamp)
}

// Log Finish Without Mutex 
func (p WithoutMutex) Finish() { 

	fmt.Printf(m.ComReqFinish, p.RequestId, p.Timestamp)
}

var startRequestTime = make(chan int64, 2)

// Start & Finish Type With Mutex
type WithMutex struct {  
	RequestId int
	Timestamp int64
	Channel chan bool // Set Channel Type Bool for mutex
}

// Log Start With Mutex 
func (p WithMutex) Start() { 
	p.Channel <- true // Lock Channel With true
	startRequestTime <- p.Timestamp // Adding Starting Time
	fmt.Printf(m.ComReqStart, p.RequestId, p.Timestamp)
}

// Log Finish With Mutex 
func (p WithMutex) Finish() { 
	fmt.Printf(m.ComReqAnswer, p.RequestId , <-startRequestTime,p.Timestamp)  // Reading Starting Time
	// fmt.Printf(m.ComReqFinish, p.RequestId, p.Timestamp)
	<-p.Channel // Unlock Channel 
}

