package request

import (
	"fmt"
	"sync"
	"time"
	"math/rand"
	log "jp_mokatest/logger"
	m "jp_mokatest/message"
)

// Request Without Mutex
func RequestWithoutMutex(reqNo int) {
	fmt.Printf(m.ComStart,reqNo)
	no := reqNo
	var wg sync.WaitGroup
	var pc Processing
	
	for i := 0; i < no; i++ {
		wg.Add(1)
		param := WithoutMutex{i,&wg}
		pc = param
		go pc.Process() // Send Request Concurrently
	}
	wg.Wait()
	fmt.Println(m.ComFinish)
}

// Request With Mutex
var ch = make(chan bool, 1)

func RequestWithMutex(reqNo int){
	fmt.Printf(m.ComStart,reqNo)
	no := reqNo
	var wg sync.WaitGroup
	var pc Processing
	
	for i := 0; i < no; i++ {
		wg.Add(1)
		param := WithMutex{i,&wg,ch}
		pc = param
		go pc.Process() // Send Request Concurrently
	}
	wg.Wait()
	fmt.Println(m.ComFinish)

}

// Get Timestamp
func TimeNow() int64 {
	TimeStart := time.Now() // Get Time Now
	return TimeStart.Unix() // Convert to int64
}

// Interface For Processing Request
type Processing interface {  
    Process()
}
// Type Without Mutex
type WithoutMutex struct {  
	requestId  int
	wg *sync.WaitGroup	
}
// Type With Mutex
type WithMutex struct {  
	requestId  int
	wg *sync.WaitGroup	
	ch chan bool // Mutex using Channel 
}

// Running Process Without Mutex 
// Randomly Process Time
func (p WithoutMutex) Process() { 

	valueStart := log.WithoutMutex{p.requestId,TimeNow()} // set request id & start now
	valueStart.Start()

	random := rand.Int31n(5) // create random time process 5 sec range
	time.Sleep(time.Duration(random) * time.Second)// process time different load

	valueFinish := log.WithoutMutex{p.requestId,TimeNow()} // set request id & Finish now
	valueFinish.Finish()
	p.wg.Done()
}


// Running Process With Mutex 
// Randomly Process Time
func (p WithMutex) Process() { 
	
	valueStart := log.WithMutex{p.requestId,TimeNow(),p.ch} // set request id & start now
	valueStart.Start()

	random := rand.Int31n(5) // create random time process 5 sec range
	time.Sleep(time.Duration(random) * time.Second)// process time different load

	valueFinish := log.WithMutex{p.requestId,TimeNow(),p.ch} // set request id & Finish now
	valueFinish.Finish()
	p.wg.Done()
}


