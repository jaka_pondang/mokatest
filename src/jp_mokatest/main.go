package main

import (
	"fmt"
	"os"
	"strconv"
	r "jp_mokatest/request"
	m "jp_mokatest/message"
)

// Main Function
func main() {
	if err := Run(os.Args[1:]...); err != nil {
		fmt.Println(err.Error())
	}
	
}

// Run Command Request
func Run(args ...string) error {
	cmdTotal := len(args)
	switch {
	case cmdTotal == 2:
		// Check Request param 2
		reqNo, Err := strconv.Atoi(args[1]) // Change to int
		if Err != nil {
			return m.ErrSecondArg
		}
		// Check Request param 1
		switch args[0] {
		case "req_n_mutex": // Without mutex
			r.RequestWithoutMutex(reqNo)
		case "req_w_mutex": // With Mutex
			r.RequestWithMutex(reqNo)
		case "help": // With Mutex
			return m.Help
		default: //default case
			return m.ErrWrongArg
		}

	default:
		return m.Help
	}

	return nil
}
